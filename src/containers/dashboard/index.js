import React, { useState } from 'react'
import FacebookLogin from 'react-facebook-login'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'
import TwitterLogin from "react-twitter-login"
import { fbLoginSuccess, fbListPagesRequest } from '../../actions/fbActions'
import Checkbox from '../../components/checkbox'
import { FaFacebook, FaCheck } from 'react-icons/fa'
import { union, omit, pull } from 'lodash'


const Dashboard = (props) => {
    const { fbAuth, fbPages } = props

    const handleResponse = (response) => {
        console.log("Response came is :", response);
        props.fbLoginSuccess(response)
    }
    const authHandler = (err, data) => {
        console.log(err, data);
    }
    const [selectedPlatforms, setPlatForms] = useState({
        fb: [],
        twitter: '',

    })

    return (
        <div className="App">
            <header style={{ display: 'flex', flexDirection: 'row' }} className="App-header">
                <div style={{ flex: 1, margin: 20 }}>
                    {isEmpty(fbAuth) ?
                        <FacebookLogin
                            appId='222265698906926'
                            callback={handleResponse}
                            isDisabled={!isEmpty(fbAuth)}
                            scope="manage_pages,publish_pages"
                        /> :
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <div style={{ margin: 5, display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <FaFacebook size={38} color="#4267b2" style={{ marginRight: 10 }} />
                                <div>
                                    <p style={{ fontWeight: 'bold', fontSize: 18, marginRight: 10 }}>{fbAuth.name}</p>
                                </div>
                                <FaCheck size={18} color="#4CAF50" />
                            </div>
                            <div style={{ backgroundColor: 'rgba(0,0,0,0.2)', borderRadius: 20, marginRight: 20, marginLeft: 20 }}>
                                {
                                    isEmpty(fbAuth) ? null :
                                        isEmpty(fbPages) ?
                                            <button
                                                title="Get Pages list"
                                                onClick={() => {
                                                    props.fbListPagesRequest()
                                                }}
                                            >List Pages</button> :
                                            fbPages.map(page => (
                                                <div key={page.id} style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
                                                    <div style={{ display: 'flex', justifyContent: 'flex-end', flex: 3 }}>
                                                        <p style={{ color: "#DDD", fontSize: 18 }}>{page.name}</p>
                                                    </div>
                                                    <div style={{ display: 'flex', justifyContent: 'center', flex: 2 }}>
                                                        <Checkbox defaultCheck={false} onCheck={(check) => {
                                                            console.log(selectedPlatforms)
                                                            if (check) {
                                                                var prevSelection = Object.assign([], selectedPlatforms.fb, [page.id])
                                                                // var currentSelection = union(selectedPlatforms.fb, page.id)
                                                                setPlatForms({ ...selectedPlatforms, fb: prevSelection })
                                                                console.log(prevSelection)
                                                            } else {
                                                                var currentSelection = pull(selectedPlatforms.fb, page.id)
                                                                setPlatForms({ ...selectedPlatforms, fb: currentSelection })
                                                                console.log(currentSelection)
                                                            }

                                                        }} />
                                                    </div>
                                                </div>
                                            ))
                                }
                            </div>
                        </div>
                    }
                </div>
                <div style={{ flex: 1, margin: 20 }}>
                    {/* <button onClick={() => {
                        console.log(selectedPlatforms)
                    }}>
                        Click
                    </button> */}
                    <TwitterLogin
                        authCallback={authHandler}
                        consumerKey="UoTKwC9gHIeLvAye1FwzVOcQP"
                        consumerSecret="oeh8EuIcucD96qQpzxSAHKFXLZkCHlO6BDSenRODyankwa3c6X"
                    />
                </div>
                <div style={{ flex: 1, margin: 20 }}>

                </div>
            </header>
            <div>

            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    fbAuth: state.fb.auth,
    fbPages: state.fb.pages,
})

export default connect(mapStateToProps, { fbLoginSuccess, fbListPagesRequest })(Dashboard) 