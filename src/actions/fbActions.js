import {
    TYPE_FB_LOGIN_SUCCESS,
    TYPE_FB_LOGIN_FAILED,
    TYPE_FB_PAGE_LOAD_SUCCESS,
    TYPE_FB_PAGE_SELECTED,
} from './actionTypes'
import { listPagesFB } from '../api'

export const fbLoginSuccess = (response) => {
    if (response.status === "unknown") {
        return {type: TYPE_FB_LOGIN_FAILED}
    } else {
        return { type: TYPE_FB_LOGIN_SUCCESS, payload: response }
    }
}

export const fbListPagesRequest = () => {
    return (dispatch, getState) => {
        const { accessToken } = getState().fb.auth
        const response = listPagesFB(accessToken).then(resp => {
            dispatch({type: TYPE_FB_PAGE_LOAD_SUCCESS, payload: resp.data.accounts.data})
        })
    }
}