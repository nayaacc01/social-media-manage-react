import React, { useState, useEffect } from 'react'
// import { useTransition, useSpring, animated } from 'react-spring'
import { FaCheck } from 'react-icons/fa'

const CheckBox = (props) => {
    const [checked, setChecked] = useState(false)
    useEffect(() => {
        if(props.defaultCheck){
            setChecked(true)
        }
    }, [])

    return (
        <div style={{cursor: 'pointer', display: 'flex'}} onClick={() => {
            props.onCheck(!checked)
            setChecked(!checked)
        }}>
            {checked ?
                <div>
                    <div style={styles.checked}>
                        <FaCheck color="white" size={18} />
                    </div>
                </div> :
                <div>
                    <div style={styles.unchecked}>

                    </div>
                </div>
            }
        </div>
    )
}

const styles = {
    checked: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#46D159',
        borderRadius: 5,
        width: 30,
        height: 30,
    },
    unchecked: {
        borderRadius: 5,
        width: 30,
        height: 30,
        borderWidth: 1,
        borderStyle: 'double',
        borderColor: 'rgba(0,0,0,0.1)',
        backgroundColor: "#FFF"
    }
}

export default CheckBox