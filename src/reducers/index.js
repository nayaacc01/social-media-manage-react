import React from 'react'
import {
    TYPE_FB_LOGIN_SUCCESS,
    TYPE_FB_PAGE_LOAD_SUCCESS,
} from '../actions/actionTypes'
import { combineReducers } from 'redux'

const initFbState = {
    auth: {},
    pages: [],
}

const fbReducers = (state=initFbState, action) => {
    switch(action.type){
        case TYPE_FB_LOGIN_SUCCESS:
            return {
                ...state,
                auth: action.payload
            }
        case TYPE_FB_PAGE_LOAD_SUCCESS:
            return {
                ...state,
                pages: action.payload
            }
        default:
            return state
    }
}


const rootReducer = combineReducers({
    fb: fbReducers
})

export default rootReducer 