import Axios from 'axios'

// fetch('https://graph.facebook.com/me?fields=id,name,accounts&access_token=EAADKJkQHcy4BAPUWKN62V4oT7d6RZAZBMjZCzPLAsZAjxIaVZC4jzHhWZBhAvZAZAmDLGDpk61GQ0qcXeE5A1N8at9MrZBvzwFLRDTvmMNbLVCWZATtfb7D9oi3CxMl0XMbQinZCdEhOF5M3De7rTgV84q35ClEqN4FTOv29hoolMUJveFtWZCZAUqGSRjji8cZCrO17e4lN2hJxK5ZAwZDZD',
//     { method: 'GET' }
// ).then((resp) => {
//     return resp.json()
// }).then((r) => {
//     console.log(r)
// }).catch(error => {
//     console.log(error)
// })

const BASE_URL = "https://graph.facebook.com/"
const base = Axios.create({
    baseURL: BASE_URL
})


export const listPagesFB = (access_token) => base.get(`me?fields=id,name,accounts&access_token=${access_token}`)

export const postToPageFB = (access_token, page_id, message, picture, link, schedule) => {
    var fullUrl = `${page_id}/feed?message=${message}`
    if (link) {
        fullUrl+=`&link=${link}`
    }
    if (schedule) {
        fullUrl += `&published=false&scheduled_publish_time=${schedule}`    
    }

    fullUrl += `&access_token=${access_token}`
    return base.post(fullUrl)
}

export const postPhotoToFB = (access_token, page_id, message, pictures, schedule) => {
    var fullUrl = `${page_id}/photos`
    pictures.forEach(picture => {
        fullUrl+= `&url=${picture}`
    })
    if(message){
        fullUrl+=`&message=${message}`
    }
    if(schedule){
        fullUrl+= `&published=false&scheduled_publish_time=${schedule}`
    }
    
    fullUrl += `&access_token=${access_token}`
    return base.post(fullUrl)
}